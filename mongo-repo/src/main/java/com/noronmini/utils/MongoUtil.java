package com.noronmini.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.model.Updates;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.mongodb.client.model.Updates.set;

public class MongoUtil {

    public static <P> Document convertToDocument(P p) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String json = objectMapper.writeValueAsString(p);
            return Document.parse(json);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static <P> P convertToPojo(Document document, Class<P> pojo) {
        System.out.println();
        Map<String, Object> map = document.entrySet()
                .stream()
                .filter(e -> e.getValue() != null)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        ObjectMapper mapper = new ObjectMapper();
        try {
            String json = mapper.writeValueAsString(map);
            return mapper.readValue(json, pojo);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Bson combineDocument(Document document) {
        List<Bson> documents = new ArrayList<>();
        document.forEach((key, value) -> {
            if (value != null) documents.add(set(key, value));
        });
        return Updates.combine(documents);
    }
}
