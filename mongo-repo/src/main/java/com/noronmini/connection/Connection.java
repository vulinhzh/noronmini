package com.noronmini.connection;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Connection {

    //String uri = "mongodb://root:Giocuanang@localhost:27017/?serverSelectionTimeoutMS=5000&connectTimeoutMS=10000";
    String uri = "mongodb://root:Giocuanang@localhost:27017/?serverSelectionTimeoutMS=5000&connectTimeoutMS=10000&authSource=postmanager&authMechanism=SCRAM-SHA-256";

    @Bean
    public MongoClient mongoClient() {
        MongoClient mongoClient = MongoClients.create(uri);
        return mongoClient;
    }

    @Bean
    public MongoDatabase mongoDatabase() {
        return mongoClient().getDatabase("noronmini");
    }

    @Bean
    public MongoCollection<Document> postCollection() {
        return mongoDatabase().getCollection("Post");
    }

    @Bean
    public MongoCollection<Document> commentCollection() {
        return mongoDatabase().getCollection("Comment");
    }
}
