package com.noronmini.repo;

import com.mongodb.client.MongoCollection;
import com.noronmini.mongo.pojo.Post;
import org.bson.Document;
import org.springframework.stereotype.Repository;

@Repository
public class PostRepo extends BaseMongoRepo<Post, Integer> {

    public PostRepo(MongoCollection<Document> postCollection) {
        collection = postCollection;
    }
}
