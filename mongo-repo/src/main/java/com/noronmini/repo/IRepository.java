package com.noronmini.repo;

import java.util.List;

public interface IRepository<P, ID> {
    List<P> findAll();

    P findById(ID id);

    // insert...
    void insertOne(P p);

    void updateById(P p, ID id);

    int deleteById(ID id);
}
