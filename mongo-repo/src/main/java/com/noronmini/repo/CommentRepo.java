package com.noronmini.repo;

import com.mongodb.client.MongoCollection;
import com.noronmini.mongo.pojo.Comment;
import com.noronmini.utils.MongoUtil;
import org.bson.Document;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class CommentRepo extends BaseMongoRepo<Comment, Integer> {

    public CommentRepo (MongoCollection<Document> commentCollection) {
        collection = commentCollection;
    }

    public int insertMany(List<Comment> comments) {
        List<Document> documents = comments.stream()
                .map(MongoUtil::convertToDocument)
                .collect(Collectors.toList());
        collection.insertMany(documents);
        return 1;
    }
}
