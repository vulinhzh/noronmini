package com.noronmini.repo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;
import com.noronmini.utils.MongoUtil;
import org.bson.Document;

import javax.annotation.PostConstruct;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseMongoRepo<P, ID> implements IRepository<P, ID> {

    protected MongoCollection<Document> collection;
    private ObjectMapper objectMapper = new ObjectMapper();
    private Class<P> pojo;

    @PostConstruct
    private void init() {
        this.pojo = (Class<P>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        System.out.println();
    }

    @Override
    public List<P> findAll() {
        return collection
                .find()
                .projection(Projections.excludeId())
                .map(document -> MongoUtil.convertToPojo(document, pojo))
                .into(new ArrayList<>());
    }

    @Override
    public P findById(ID id) {
        Document document = collection
                .find()
                .filter(Filters.eq("id", id))
                .projection(Projections.excludeId())
                .first();
        System.out.println();
        return MongoUtil.convertToPojo(document, pojo);
    }

    @Override
    public void insertOne(P p) {
        collection.insertOne(MongoUtil.convertToDocument(p));
    }

    @Override
    public void updateById(P p, ID id) {
        String json = "";
        try {
            json = objectMapper.writeValueAsString(p);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        Document document = Document.parse(json);
        collection.updateOne(Filters.eq("id", id), MongoUtil.combineDocument(document));
    }

    @Override
    public int deleteById(ID id) {
        collection.deleteOne(Filters.eq("id", id));
        return 1;
    }
}
