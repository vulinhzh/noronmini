package com.noronmini.repo;

import com.noronmini.jooq.Tables;
import com.noronmini.jooq.tables.pojos.User;
import com.noronmini.jooq.tables.records.UserRecord;
import com.noronmini.utils.MysqlUtils;
import org.jooq.Table;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public class UserRepo extends BaseRepo<UserRecord, User, Integer> {

    @Override
    protected Table<UserRecord> getTable() {
        return Tables.USER;
    }

    public List<User> findByIds(Set<Integer> userIds) {
        return dslContext
                .selectFrom(Tables.USER)
                .where(MysqlUtils.getFieldId(getTable()).in(userIds))
                .fetchInto(User.class);
    }
}
