package com.noronmini.repo;

import org.jooq.Record;

import java.util.List;

public interface IRepository<R extends Record, P, ID> {
    List<P> findAll();

    P findById(ID id);

    // insert...
    P insertOne(P p);

    int updateById(P p, ID id);

    int deleteById(ID id);
}
