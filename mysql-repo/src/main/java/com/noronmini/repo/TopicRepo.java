package com.noronmini.repo;

import com.noronmini.jooq.Tables;
import com.noronmini.jooq.tables.pojos.Topic;
import com.noronmini.jooq.tables.records.TopicRecord;
import com.noronmini.utils.MysqlUtils;
import org.jooq.Table;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Repository
public class TopicRepo extends BaseRepo<TopicRecord, Topic, Integer> {

    @Override
    protected Table<TopicRecord> getTable() {
        return Tables.TOPIC;
    }

    public List<Topic> findByIds(Set<Integer> ids) {
        return dslContext
                .selectFrom(Tables.TOPIC)
                .where(MysqlUtils.getFieldId(getTable()).in(ids))
                .fetchInto(Topic.class);
    }

    public void changeNumQuestion(Integer topicId, Long num) {
        dslContext
                .update(getTable())
                .set(Tables.TOPIC.NUM_QUESTION, Tables.TOPIC.NUM_QUESTION.plus(num))
                .where(MysqlUtils.getFieldId(getTable()).eq(topicId))
                .execute();
    }

    public void changeNumAnswer(Integer topicId, Long num) {
        dslContext
                .update(getTable())
                .set(Tables.TOPIC.NUM_ANSWER, Tables.TOPIC.NUM_ANSWER.plus(num))
                .where(MysqlUtils.getFieldId(getTable()).eq(topicId))
                .execute();
    }

    public void changeNumQuestionManyTopic(Map<Integer, Long> mapTopic) {
        mapTopic.entrySet()
                .forEach(e -> changeNumQuestion(e.getKey(), e.getValue()));
    }

    public void changeNumAnswerManyTopic(Map<Integer, Long> mapTopic) {
        mapTopic.entrySet()
                .forEach(e -> changeNumAnswer(e.getKey(), e.getValue()));
    }
}
