package com.noronmini.repo;

import com.noronmini.utils.MysqlUtils;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Table;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.util.List;

@Repository
public abstract class BaseRepo<R extends Record, P, ID> implements IRepository<R, P, ID> {

    @Autowired
    protected DSLContext dslContext;
    private Class<P> pojo;
    private R record;

    @PostConstruct
    private void init() throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        this.pojo = (Class<P>) ((ParameterizedType) this.getClass()
                .getGenericSuperclass())
                .getActualTypeArguments()[1];
        System.out.println();
        this.record = (R) ((Class) ((ParameterizedType) this.getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0])
                .getDeclaredConstructor()
                .newInstance();
        System.out.println();
    }

    protected abstract Table<R> getTable();

    @Override
    public List<P> findAll() {
        return dslContext
                .selectFrom(getTable())
                .fetchInto(pojo);
    }

    @Override
    public P findById(ID id) {
        return dslContext
                .selectFrom(getTable())
                .where(MysqlUtils.getFieldId(getTable()).eq(id))
                .fetchOneInto(pojo);
    }

    @Override
    public P insertOne(P p) {
        System.out.println();
        P newp = dslContext
                .insertInto(getTable())
                .set(MysqlUtils.mapFieldAndValue(this.record, p, getTable()))
                .returning()
                .fetchOne()
                .into(pojo);
        System.out.println();
        return newp;
    }

    @Override
    public int updateById(P p, ID id) {
        return dslContext
                .update(getTable())
                .set(MysqlUtils.mapFieldAndValue(record, p, getTable()))
                .where(MysqlUtils.getFieldId(getTable()).eq(id))
                .execute();
    }

    @Override
    public int deleteById(ID id) {
        return dslContext
                .delete(getTable())
                .where(MysqlUtils.getFieldId(getTable()).eq(id))
                .execute();
    }

//    public void test() {
//        dslContext.delete(getTable())
//                .where()
//    }
}
