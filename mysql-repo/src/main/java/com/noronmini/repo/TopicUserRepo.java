package com.noronmini.repo;

import com.noronmini.jooq.Tables;
import com.noronmini.jooq.tables.pojos.Topic;
import com.noronmini.jooq.tables.pojos.TopicUser;
import com.noronmini.jooq.tables.records.TopicUserRecord;
import com.noronmini.utils.MysqlUtils;
import org.jooq.Field;
import org.jooq.Table;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TopicUserRepo extends BaseRepo<TopicUserRecord, TopicUser, UniqueKey<TopicUserRecord>> {

    private final TopicRepo topicRepo;

    public TopicUserRepo(TopicRepo topicRepo) {
        this.topicRepo = topicRepo;
    }

    @Override
    protected Table<TopicUserRecord> getTable() {
        return Tables.TOPIC_USER;
    }

    public List<TopicUser> findByTopicId(Integer id) {
        Field<Integer> field = (Field<Integer>) getTable().field("topic_id");
        return dslContext
                .selectFrom(Tables.TOPIC_USER)
                .where(field.eq(id))
                .fetchInto(TopicUser.class);
    }

    public int deleteById(Integer userId, Integer topicId) {
        Field<Integer> userField = (Field<Integer>) getTable().field("user_id");
        Field<Integer> topicField = (Field<Integer>) getTable().field("topic_id");
        dslContext
                .delete(getTable())
                .where(DSL.row(userField, topicField).eq(userId, topicId))
                .execute();
        dslContext
                .update(Tables.TOPIC)
                .set(Tables.TOPIC.NUM_FOLLOWER, Tables.TOPIC.NUM_FOLLOWER.plus(-1))
                .where(MysqlUtils.getFieldId(Tables.TOPIC).eq(topicId))
                .execute();
        return 1;
    }

    @Override
    public TopicUser insertOne(TopicUser topicUser) {
//        TopicUser newT_U = super.insertOne(topicUser);POST
        System.out.println();
        dslContext
                .insertInto(Tables.TOPIC_USER)
                .set(MysqlUtils.mapFieldAndValue(getTable().newRecord(), topicUser, getTable()))
                .execute();
        dslContext
                .update(Tables.TOPIC)
                .set(Tables.TOPIC.NUM_FOLLOWER, Tables.TOPIC.NUM_FOLLOWER.plus(1))
                .where(MysqlUtils.getFieldId(Tables.TOPIC).eq(topicUser.getTopicId()))
                .execute();
        System.out.println();
        return topicUser;
    }
}
