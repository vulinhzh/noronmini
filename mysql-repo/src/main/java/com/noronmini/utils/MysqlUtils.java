package com.noronmini.utils;

import org.jooq.Field;
import org.jooq.Record;
import org.jooq.Table;
import org.jooq.UniqueKey;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class MysqlUtils {
    public static <ID> Field<ID> getFieldId(Table<? extends Record> table) {
        return (Field<ID>) table.field("id");
    }

//    public static <R extends Record> Field<?> getFiedldId() {
//        Object UniqueKey = new UniqueKey<R>() {};
//        return UniqueKey<R>
//    }

    public static <P> Map<Field<?>, Object> mapFieldAndValue(Record record, P pojo, Table<? extends Record> table) {
        record.from(pojo);
        Map<Field<?>, Object> map = new HashMap<>();
        Arrays.stream(table.fields())
                .forEach(e -> {
                    if (record.get(e) != null) map.put(e, record.get(e));
                });
        System.out.println();
        return map;
    }
}
