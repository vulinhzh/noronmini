package com.noronmini.kafkadata;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class NoronMessage {
    private String objectType;
    private String actionType;
    private Long createdAt;

    // Comment Info
    private Integer commentId;
    //Post Info
    private Integer postId;
    private List<Integer> topicIds;
}
