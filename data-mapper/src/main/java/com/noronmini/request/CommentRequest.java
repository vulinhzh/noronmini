package com.noronmini.request;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class CommentRequest {
    private Integer id;
    private String content;
    private Integer creatorId;
    private Integer postId;
    private Integer topicId;
    private Integer parentId;
}
