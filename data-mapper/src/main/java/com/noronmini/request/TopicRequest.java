package com.noronmini.request;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class TopicRequest {
    private Integer id;
    private String name;
    private String imgUrl;
    private String description;
}
