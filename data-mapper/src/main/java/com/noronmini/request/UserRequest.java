package com.noronmini.request;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UserRequest {
    private Integer id;
    private String username;
    private String password;
    private String avatarUrl;
    private String name;
}
