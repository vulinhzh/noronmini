package com.noronmini.request;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class PostRequest {
    private Integer id;
    private String title;
    private String content;
    private String type;
    private Integer creatorId;
    private List<Integer> topicIds;
}
