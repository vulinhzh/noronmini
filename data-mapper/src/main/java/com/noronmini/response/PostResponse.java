package com.noronmini.response;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class PostResponse {
    private Integer id;
    private String title;
    private String content;
    private String type;
    private Integer numComments;
    private Integer numLikes;
    private Integer numViews;
    private Integer creatorId;
    private UserResponse creator;
    private List<Integer> topicIds;
    private List<TopicName> topics;
    private Long createdAt;
    private Long updatedAt;
    private Long deletedAt;
}
