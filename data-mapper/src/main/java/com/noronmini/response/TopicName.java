package com.noronmini.response;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class TopicName {
    private Integer id;
    private String name;
}
