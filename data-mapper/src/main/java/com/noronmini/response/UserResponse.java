package com.noronmini.response;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UserResponse {
    private Integer id;
    private String username;
    private String avatarUrl;
    private Long createdAt;
    private Long updatedAt;
    private Long deletedAt;
    private String name;
}
