package com.noronmini.response;

import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
public class TopicResponse {
    private Integer id;
    private String name;
    private String imgUrl;
    private String description;
    private Integer numFollower;
    private Integer numPost;
    private Integer numAnswer;
    private Integer numQuestion;
    private Long createdAt;
    private Long updatedAt;
    private Long deletedAt;
}
