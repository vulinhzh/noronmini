package com.noronmini.response;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class CommentResponse {
    private Integer id;
    private String content;
    private Integer creatorId;
    private UserResponse creator;
    private Integer numLikes;
    private Integer parentId;
    private Long createdAt;
    private Long updatedAt;
    private Long deletedAt;
}
