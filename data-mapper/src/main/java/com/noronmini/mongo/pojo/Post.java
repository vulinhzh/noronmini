package com.noronmini.mongo.pojo;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class Post {
    private Integer id;
    private String title;
    private String content;
    private String type;
    private Integer numComments = 0;
    private Integer numLikes = 0;
    private Integer numViews = 0;
    private Integer creatorId;
    private List<Integer> topicIds;
    private Long createdAt;
    private Long updatedAt;
    private Long deletedAt;
}
