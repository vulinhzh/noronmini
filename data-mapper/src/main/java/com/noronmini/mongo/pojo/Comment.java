package com.noronmini.mongo.pojo;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class Comment {
    private Integer id;
    private String content;
    private Integer creatorId;
    private Integer numLikes = 0;
    private Integer postId;
    private Integer topicId;
    private Integer parentId;
    private Long createdAt;
    private Long updatedAt;
    private Long deletedAt;
}
