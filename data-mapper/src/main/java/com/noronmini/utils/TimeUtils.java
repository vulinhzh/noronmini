package com.noronmini.utils;

import java.time.*;
import java.util.Date;

public class TimeUtils {
    public static Long generateMilis(LocalDateTime time) {
        if (time != null)
            return time.toEpochSecond(ZoneOffset.UTC);
        else return null;
    }

    public static Long generateMilis(LocalDate time) {
        if (time != null)
            return time.atStartOfDay(ZoneOffset.UTC).toInstant().toEpochMilli();
        else return null;
    }

    public static Date asDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }

    public static Date asDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static LocalDate asLocalDate(Date date) {
        return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static LocalDateTime asLocalDateTime(Date date) {
        return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDateTime();
    }
}
