package com.noronmini.mapper;

import com.noronmini.mongo.pojo.Comment;
import com.noronmini.request.CommentRequest;
import com.noronmini.response.CommentResponse;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public abstract class CommentMapper extends AbstractMapper<Comment, CommentRequest, CommentResponse> {

}
