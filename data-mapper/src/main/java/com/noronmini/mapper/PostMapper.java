package com.noronmini.mapper;

import com.noronmini.mongo.pojo.Post;
import com.noronmini.request.PostRequest;
import com.noronmini.response.PostResponse;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public abstract class PostMapper extends AbstractMapper<Post, PostRequest, PostResponse> {
//    @AfterMapping
//    void mapOfResponse(@MappingTarget PostResponse postResponse, @Context User user, @Context List<Topic> topics) {
//        postResponse.setCreator(userMapper.toResponse(user));
//        List<TopicName> topicNames = topics.stream()
//                .map(e -> topicMapper.toTopicName(e))
//                .collect(Collectors.toList());
//        postResponse.setTopics(topicNames);
//    }
//
//    public List<PostResponse> toPostResponses
//            (List<Post> posts, Map<Integer, User> mapUser, Map<Integer, Topic> mapTopic) {
//        return posts.stream()
//                .map(post -> {
//                    List<Topic> topics = post.getTopicIds().stream()
//                            .map(e -> mapTopic.get(e))
//                            .collect(Collectors.toList());
//                    return toPostResponse(post, mapUser.get(post.getCreatorId()), topics);
//                })
//                .collect(Collectors.toList());
//    }
}
