package com.noronmini.mapper;

import com.noronmini.kafkadata.NoronMessage;
import com.noronmini.mongo.pojo.Comment;
import com.noronmini.mongo.pojo.Post;
import com.noronmini.request.CommentRequest;
import com.noronmini.request.PostRequest;
import org.mapstruct.*;

import java.util.ArrayList;
import java.util.Date;

@Mapper(componentModel = "spring")
public abstract class MessageMapper {

    @BeanMapping(qualifiedByName = "post")
    @Mappings(
            @Mapping(target = "postId", source = "postRequest.id")
    )
    public abstract NoronMessage toMessage(PostRequest postRequest, @Context String actionType);

    @BeanMapping(qualifiedByName = "post")
    @Mappings(
            {@Mapping(target = "postId", source = "post.id"),
            @Mapping(target = "createdAt", ignore = true)}
    )
    public abstract NoronMessage toMessage(Post post, @Context String actionType);

    @Named("post")
    @AfterMapping
    protected void addMoreWithPost(@MappingTarget NoronMessage noronMessage, @Context String actionType) {
        noronMessage.setActionType(actionType);
        noronMessage.setObjectType("Post");
        noronMessage.setCreatedAt(new Date().getTime());
    }

    @Mappings(
            @Mapping(target = "commentId", source = "commentRequest.id")
    )
    public abstract NoronMessage toMessage(CommentRequest commentRequest, @Context String actionType);

    @Mappings(
            @Mapping(target = "commentId", source = "comment.id")
    )
    public abstract NoronMessage toMessage(Comment comment, @Context String actionType);

    @AfterMapping
    protected void addMoreWithComment(@MappingTarget NoronMessage noronMessage, Comment comment, @Context String actionType) {
        afterMapComment(noronMessage, actionType);
        noronMessage.setTopicIds(new ArrayList<>());
        noronMessage.getTopicIds().add(comment.getTopicId());
    }

    @AfterMapping
    protected void addMoreWithComment(@MappingTarget NoronMessage noronMessage, CommentRequest comment, @Context String actionType) {
        afterMapComment(noronMessage, actionType);
        noronMessage.setTopicIds(new ArrayList<>());
        noronMessage.getTopicIds().add(comment.getTopicId());
    }

    protected void afterMapComment(NoronMessage noronMessage, String actionType) {
        noronMessage.setActionType(actionType);
        noronMessage.setObjectType("Comment");
        noronMessage.setCreatedAt(new Date().getTime());
        noronMessage.setTopicIds(new ArrayList<>());
    }
}
