package com.noronmini.mapper;

import com.noronmini.jooq.tables.pojos.User;
import com.noronmini.request.UserRequest;
import com.noronmini.response.UserResponse;
import com.noronmini.utils.TimeUtils;
import org.mapstruct.*;

@Mapper(componentModel = "spring")
public abstract class UserMapper extends AbstractMapper<User, UserRequest, UserResponse>{

    @Override
    @Mappings( value = {
            @Mapping(target = "createdAt", ignore = true),
            @Mapping(target = "updatedAt", ignore = true),
            @Mapping(target = "deletedAt", ignore = true)
    })
    public abstract UserResponse toResponse(User user);

    @AfterMapping
    void addTimeToResponseUser(@MappingTarget UserResponse responseUser, User user) {
        responseUser.setCreatedAt(TimeUtils.generateMilis(user.getCreatedAt()));
        responseUser.setUpdatedAt(TimeUtils.generateMilis(user.getUpdatedAt()));
        responseUser.setDeletedAt(TimeUtils.generateMilis(user.getDeletedAt()));
    }


}
