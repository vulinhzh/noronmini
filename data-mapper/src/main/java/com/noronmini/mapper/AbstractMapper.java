package com.noronmini.mapper;

import java.util.List;

public abstract class AbstractMapper<Po, Rq, Rs> {

    public abstract Po toPOJO(Rq rq);

    public abstract Rs toResponse(Po po);

    public abstract List<Rs> toListResponse(List<Po> pos);

}
