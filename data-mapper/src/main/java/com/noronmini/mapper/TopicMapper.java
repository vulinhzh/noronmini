package com.noronmini.mapper;

import com.noronmini.jooq.tables.pojos.Topic;
import com.noronmini.request.TopicRequest;
import com.noronmini.response.TopicName;
import com.noronmini.response.TopicResponse;
import com.noronmini.utils.TimeUtils;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public abstract class TopicMapper extends AbstractMapper<Topic, TopicRequest, TopicResponse> {

    public abstract TopicName toTopicName(Topic topic);

    public abstract List<TopicName> toListTopicName(List<Topic> topicList);

    @Override
    @Mappings(value = {
            @Mapping(target = "createdAt", ignore = true),
            @Mapping(target = "updatedAt", ignore = true),
            @Mapping(target = "deletedAt", ignore = true)
    })
    public abstract TopicResponse toResponse(Topic topic);

    @AfterMapping
    void addTimeToResponse(@MappingTarget TopicResponse topicResponse, Topic topic) {
        topicResponse.setCreatedAt(TimeUtils.generateMilis(topic.getCreatedAt()));
        topicResponse.setUpdatedAt(TimeUtils.generateMilis(topic.getUpdatedAt()));
        topicResponse.setDeletedAt(TimeUtils.generateMilis(topic.getDeletedAt()));
    }
}
