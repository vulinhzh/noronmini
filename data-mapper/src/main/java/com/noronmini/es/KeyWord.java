package com.noronmini.es;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class KeyWord {
    private String keyword;
    private String field;
}
