import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.noronmini.*"})
public class DataMapperApplication {
    public static void main(String[] args) {
        SpringApplication.run(DataMapperApplication.class, args);
    }
}
