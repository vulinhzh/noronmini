package com.noronmini.logger;

import com.noronmini.annotation.Identity;
import com.noronmini.kafkadata.NoronMessage;
import com.noronmini.service.ESService;
import com.noronmini.service.TopicConsumer;

import java.util.List;

@Identity(value = "post_insert")
public class NewPost implements ILogger {
    private final TopicConsumer topicConsumer;
    private final ESService esService;
    public NewPost(TopicConsumer topicConsumer, ESService esService) {
        this.topicConsumer = topicConsumer;
        this.esService = esService;
    }

    @Override
    public void handle(List<NoronMessage> messages) {
        topicConsumer.changeNumQuestion(messages, false);
        esService.postWithBulk(messages);
        System.out.println("change num question");
    }
}
