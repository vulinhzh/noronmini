package com.noronmini.logger;

import com.noronmini.annotation.Identity;
import com.noronmini.kafkadata.NoronMessage;
import com.noronmini.service.TopicConsumer;

import java.util.List;

@Identity("comment_insert")
public class IncreaseNumAnswer implements ILogger {

    private final TopicConsumer topicConsumer;

    public IncreaseNumAnswer(TopicConsumer topicConsumer) {
        this.topicConsumer = topicConsumer;
    }

    @Override
    public void handle(List<NoronMessage> messages) {
        topicConsumer.changeNumAnswer(messages, false);
        System.out.println("change num answer");
    }
}
