package com.noronmini.logger;

import com.noronmini.annotation.Identity;
import lombok.Getter;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.*;

@Component
@Getter
public class LoggerFactory {
    private Map<String, List<ILogger>> KEY_IMPL;

    public LoggerFactory(List<ILogger> iLoggers) {
        System.out.println();
        this.KEY_IMPL = iLoggers.stream()
                .filter(handler -> handler.getClass().isAnnotationPresent(Identity.class))
                .flatMap(handler -> Arrays
                        .stream(handler.getClass().getAnnotation(Identity.class).value())
                        .map(iden -> Pair.of(iden, handler)))
                .collect(groupingBy(Pair::getLeft, mapping(Pair::getRight, toList())));
    }
}
