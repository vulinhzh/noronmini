package com.noronmini.logger;

import com.noronmini.kafkadata.NoronMessage;

import java.util.List;

public interface ILogger {
    void handle(List<NoronMessage> messages);
}
