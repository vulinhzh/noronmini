package com.noronmini.logger;

import com.noronmini.annotation.Identity;
import com.noronmini.kafkadata.NoronMessage;
import com.noronmini.service.TopicConsumer;

import java.util.List;

@Identity("comment_delete")
public class DecreaseNumAnswer implements ILogger {

    private final TopicConsumer topicConsumer;

    public DecreaseNumAnswer(TopicConsumer topicConsumer) {
        this.topicConsumer = topicConsumer;
    }

    @Override
    public void handle(List<NoronMessage> messages) {
        topicConsumer.changeNumAnswer(messages, true);
        System.out.println("change num answer");
    }
}
