//package com.noronmini.service;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.noronmini.mongo.pojo.Comment;
//import com.noronmini.repo.CommentRepo;
//import com.noronmini.repo.TopicRepo;
//
//import java.util.List;
//import java.util.stream.Collectors;
//
//public class CommentConsumer {
//    private final CommentRepo commentRepo;
//    private final TopicRepo topicRepo;
//    private ObjectMapper objectMapper = new ObjectMapper();
//
//    public CommentConsumer(CommentRepo commentRepo, TopicRepo topicRepo) {
//        this.commentRepo = commentRepo;
//        this.topicRepo = topicRepo;
//    }
//
//    public void increaseNumAnswer(List<Comment> comments) {
//        comments.stream()
//                .collect(Collectors.groupingBy(Comment::getTopicId, Collectors.counting()))
//                .forEach((topicId, num) -> topicRepo.increaseNumAnswer(topicId, num.intValue()));
//    }
//}
