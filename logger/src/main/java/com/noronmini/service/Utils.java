package com.noronmini.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.noronmini.kafkadata.NoronMessage;
import com.noronmini.mongo.pojo.Post;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Utils {
    private ObjectMapper objectMapper = new ObjectMapper();

    public static  <P> P mapToObj(String json, Class<P> clazz) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(json, clazz);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Map<Integer, Long> toMapTopic(List<NoronMessage> messages) {
        List<Integer> list = new ArrayList<>();
        messages.stream()
                .map(NoronMessage::getTopicIds)
                .forEach(e -> e.stream().forEach(k -> list.add(k)));
        return list.stream()
                .collect(Collectors.groupingBy(e -> e, Collectors.counting()));
    }

//    public static
}
