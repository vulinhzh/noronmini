package com.noronmini.service;

import com.noronmini.kafkadata.NoronMessage;
import com.noronmini.repo.TopicRepo;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class TopicConsumer {
    private final TopicRepo topicRepo;

    public TopicConsumer(TopicRepo topicRepo) {
        this.topicRepo = topicRepo;
    }

    public void changeNumAnswer(List<NoronMessage> messages, @Nullable Boolean isDecrease) {
        Map<Integer, Long> map = Utils.toMapTopic(messages);
        if (isDecrease != null && isDecrease == true) {
            map.forEach((k, v) -> {
                map.replace(k, map.get(k), - map.get(k));
            });
        }
        topicRepo.changeNumAnswerManyTopic(map);
    }

    public void changeNumQuestion(List<NoronMessage> messages,@Nullable Boolean isDecrease) {
        Map<Integer, Long> map = Utils.toMapTopic(messages);
        if (isDecrease != null && isDecrease == true) {
            map.forEach((k, v) -> {
                map.replace(k, map.get(k), - map.get(k));
            });
        }
        topicRepo.changeNumQuestionManyTopic(map);
    }
}
