package com.noronmini.consumer;

import com.noronmini.kafkadata.NoronMessage;
import com.noronmini.logger.ILogger;
import com.noronmini.logger.LoggerFactory;
import com.noronmini.service.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ConsumerNoron {

    @Autowired
    private LoggerFactory loggerFactory;

    @PostConstruct
    void init() {
        System.out.println();
    }

    @KafkaListener(
            topics = "noronmini",
            groupId = "noron-group",
            batch = "true",
            concurrency = "2", properties = "consumerFactory")
    public void listener(List<String> jsons) {
        Map<String, List<ILogger>> KEY_IMPL = loggerFactory.getKEY_IMPL();
        Map<String, List<NoronMessage>> map = jsons.stream()
                .map(e -> Utils.mapToObj(e, NoronMessage.class))
                .collect(Collectors.groupingBy(NoronMessage::getActionType));
        map.forEach((k, v) -> {
            List<ILogger> list = KEY_IMPL.get(k);
            list.stream()
                    .forEach(e -> e.handle(v));
        });
    }

}
