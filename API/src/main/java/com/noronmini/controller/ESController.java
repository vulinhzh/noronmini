package com.noronmini.controller;

import com.noronmini.es.KeyWord;
import com.noronmini.service.ESService;
import com.noronmini.service.elasticsearch.ESServiceContr;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ESController {

    private final ESServiceContr esService;
    private final ESService service;

    public ESController(ESServiceContr esService, ESService service) {
        this.esService = esService;
        this.service = service;
    }

    @GetMapping(value = "/api/v1/es/{id}")
    public ResponseEntity getOne(@PathVariable(name = "id") String id) {
        return ResponseEntity.ok(esService.getOne(id));
    }

    @GetMapping(value = "/api/v1/es/search")
    public ResponseEntity searchByKeyWord(@RequestBody KeyWord keyWord) {
        return ResponseEntity.ok(esService.searchPost(keyWord));
    }

    @GetMapping(value = "/api/v1/es/search2")
    public ResponseEntity searchByKeyWord2(@RequestBody KeyWord keyWord) {
        return ResponseEntity.ok(service.searchByKeyword2(keyWord.getKeyword(), keyWord.getField()));
    }

}
