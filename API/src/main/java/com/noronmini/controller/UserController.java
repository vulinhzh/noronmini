package com.noronmini.controller;

import com.noronmini.request.UserRequest;
import com.noronmini.service.mysql.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/api/v1/users", method = RequestMethod.GET)
    public ResponseEntity viewUser() {
        return ResponseEntity
                .ok(userService.getAll());
    }

    @SuppressWarnings("rawtypes")
    @RequestMapping(value = "/api/v1/users", method = RequestMethod.POST)
    public ResponseEntity addNewUser(@RequestBody UserRequest user) {
        return ResponseEntity
                .ok(userService.insertOne(user));
    }

    @RequestMapping(value = "/api/v1/users/{id}", method = RequestMethod.PUT)
    public ResponseEntity updateUser(@PathVariable(name = "id") Integer id, @RequestBody UserRequest user) {
        return ResponseEntity
                .ok(userService.updateById(user, id));
    }

    @RequestMapping(value = "/api/v1/users/{id}", method = RequestMethod.GET)
    public ResponseEntity findById(@PathVariable(name = "id") Integer id) {
        return ResponseEntity
                .ok(userService.getById(id));
    }

    @RequestMapping(value = "/api/v1/users/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteById(@PathVariable(name = "id") Integer id) {
        return ResponseEntity
                .ok(userService.deleteById(id));
    }

    //mongo
//    @RequestMapping(value = "/api/v2/users", method = RequestMethod.GET)
//    public ResponseEntity viewUserMG() {
//        return ResponseEntity.ok(mgUserService.getAllUser());
//    }
//
//    @RequestMapping(value = "/api/v2/users", method = RequestMethod.POST)
//    public ResponseEntity addNewUserMG(@RequestBody UserRequest user) {
//        try {
//            return ResponseEntity
//                    .ok(mgUserService.addNewUser(user));
//        } catch (Exception e) {
//            return ResponseEntity
//                    .status(HttpStatus.BAD_REQUEST)
//                    .body(new ResponseStatus(LocalDateTime.now(), "somethings wrong"));
//        }
//    }
//
//    @RequestMapping(value = "/api/v2/users/{id}", method = RequestMethod.PUT)
//    public ResponseEntity updateUserMG(@PathVariable(name = "id") Integer id, @RequestBody UserUpdateRequest user) {
//        try {
//            return ResponseEntity
//                    .ok(mgUserService.updateUser(user, id));
//        } catch (Exception e) {
//            return ResponseEntity
//                    .status(HttpStatus.BAD_REQUEST)
//                    .body(new ResponseStatus(LocalDateTime.now(), "somethings wrong"));
//        }
//    }
//
//    @RequestMapping(value = "/api/v2/users/{id}", method = RequestMethod.DELETE)
//    public ResponseEntity deleteByIdMG(@PathVariable(name = "id") Integer id) {
//        return ResponseEntity
//                .ok(mgUserService.deleteUser(id));
//    }
}
