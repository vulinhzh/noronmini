package com.noronmini.controller;

import com.noronmini.request.CommentRequest;
import com.noronmini.request.PostRequest;
import com.noronmini.response.CommentResponse;
import com.noronmini.service.mongo.CommentService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class CommentController {

    private final CommentService commentService;

    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @RequestMapping(value = "/api/v1/comments", method = RequestMethod.GET)
    public ResponseEntity viewAll(HttpServletRequest request, ModelAndView model) {
        List<CommentResponse> cmts = commentService.getAll();
        model.addObject(cmts);
        return ResponseEntity.ok(cmts);
    }

    @RequestMapping(value = "/api/v1/comments", method = RequestMethod.POST)
    public ResponseEntity addNew(@RequestBody CommentRequest cmt, Model model) {
        System.out.println();
        CommentResponse commentResponse = commentService.insertOne(cmt);
        model.addAttribute("comment", commentResponse);
        return ResponseEntity
                .ok(commentResponse);
    }

    @PutMapping(value = "/api/v1/comments/{id}")
    public ResponseEntity update(@PathVariable(name = "id") Integer id, @RequestBody CommentRequest commentRequest) {
        return ResponseEntity
                .ok(commentService.updateById(commentRequest, id));
    }

    @RequestMapping(value = "/api/v1/comments/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deletePostMG(@PathVariable(name = "id") Integer id) {
        return ResponseEntity
                .ok(commentService.deleteById(id));
    }
}
