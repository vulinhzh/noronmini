package com.noronmini.controller;

import com.noronmini.jooq.tables.pojos.TopicUser;
import com.noronmini.service.mysql.TopicService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class TopicUserController {

    private final TopicService topicService;

    public TopicUserController(TopicService topicService) {
        this.topicService = topicService;
    }

    @GetMapping(value = "/api/v1/topic/followers/{id}")
    public ResponseEntity getFollowers(@PathVariable(name = "id") Integer topicId) {
        return ResponseEntity.ok(topicService.getFollowerByPostId(topicId));
    }

    @PostMapping(value = "/api/v1/topic/followers")
    public ResponseEntity addFollower(@RequestBody TopicUser topicUser) {
        return ResponseEntity.ok(topicService.addFollower(topicUser));
    }

    @DeleteMapping(value = "/api/v1/topic/followers")
    public ResponseEntity removeFollower(@RequestBody TopicUser topicUser) {
        return ResponseEntity.ok(topicService.removeFollower(topicUser.getTopicId(), topicUser.getUserId()));
    }
}
