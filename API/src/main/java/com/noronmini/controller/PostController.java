package com.noronmini.controller;

import com.noronmini.request.PostRequest;
import com.noronmini.response.PostResponse;
import com.noronmini.service.mongo.PostService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class PostController {

    private final PostService postService;

    public PostController(PostService postService) {
        this.postService = postService;
    }

    @RequestMapping(value = "/api/v1/posts", method = RequestMethod.GET)
    public ResponseEntity viewPostMG(HttpServletRequest request) {
        List<PostResponse> posts = postService.getAll();
        return ResponseEntity.ok(posts);
    }

    @RequestMapping(value = "/api/v1/posts", method = RequestMethod.POST)
    public ResponseEntity addNewPostMG(@RequestBody PostRequest post) {
        return ResponseEntity
                .ok(postService.insertOne(post));
    }

    @PutMapping(value = "/api/v1/posts/{id}")
    public ResponseEntity updatePostMG(@PathVariable(name = "id") Integer id, @RequestBody PostRequest post) {
        return ResponseEntity
                .ok(postService.updateById(post, id));
    }

    @RequestMapping(value = "/api/v1/posts/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deletePostMG(@PathVariable(name = "id") Integer id) {
        return ResponseEntity
                .ok(postService.deleteById(id));
    }
}
