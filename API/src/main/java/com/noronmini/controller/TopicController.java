package com.noronmini.controller;

import com.noronmini.request.TopicRequest;
import com.noronmini.service.mysql.TopicService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class TopicController {
    
    private final TopicService topicService;

    public TopicController(TopicService topicService) {
        this.topicService = topicService;
    }

    @RequestMapping(value = "/api/v1/topics", method = RequestMethod.GET)
    public ResponseEntity viewAll() {
        return ResponseEntity
                .ok(topicService.getAll());
    }

    @SuppressWarnings("rawtypes")
    @RequestMapping(value = "/api/v1/topics", method = RequestMethod.POST)
    public ResponseEntity addNew(@RequestBody TopicRequest topic) {
        return ResponseEntity
                .ok(topicService.insertOne(topic));
    }

    @RequestMapping(value = "/api/v1/topics/{id}", method = RequestMethod.PUT)
    public ResponseEntity update(@PathVariable(name = "id") Integer id, @RequestBody TopicRequest topic) {
        return ResponseEntity
                .ok(topicService.updateById(topic, id));
    }

    @RequestMapping(value = "/api/v1/topics/{id}", method = RequestMethod.GET)
    public ResponseEntity findById(@PathVariable(name = "id") Integer id) {
        return ResponseEntity
                .ok(topicService.getById(id));
    }

    @RequestMapping(value = "/api/v1/topics/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteById(@PathVariable(name = "id") Integer id) {
        return ResponseEntity
                .ok(topicService.deleteById(id));
    }
}
