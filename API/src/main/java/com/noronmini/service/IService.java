package com.noronmini.service;

import java.util.List;

public interface IService<Rp, Rq, ID> {
    /*
    P : response
    Q : request
     */
    List<Rp> getAll();

    Rp insertOne(Rq c);

    Rp updateById(Rq c, ID id);

    int deleteById(ID id);

    Rp getById(ID id);
}
