package com.noronmini.service.mongo;

import com.noronmini.mapper.AbstractMapper;
import com.noronmini.repo.BaseMongoRepo;
import com.noronmini.service.IService;

import java.util.List;

public abstract class BaseService<Rs, Rq, ID, Po> implements IService<Rs, Rq, ID> {

    protected BaseMongoRepo<Po, ID> baseRepo;
    protected AbstractMapper<Po, Rq, Rs> mapper;

    @Override
    public List<Rs> getAll() {
        List<Po> list = baseRepo.findAll();
        System.out.println();
        return mapper.toListResponse(list);
    }

    @Override
    public Rs insertOne(Rq q) {
        Po po = mapper.toPOJO(q);
        baseRepo.insertOne(po);
        return mapper.toResponse(po);
    }

    @Override
    public Rs updateById(Rq q, ID id) {
        Po d = mapper.toPOJO(q);
        baseRepo.updateById(d, id);
        return mapper.toResponse(d);
    }

    @Override
    public int deleteById(ID id) {
        baseRepo.deleteById(id);
        return 1;
    }

    @Override
    public Rs getById(ID id) {
        return mapper.toResponse(baseRepo.findById(id));
    }
}

