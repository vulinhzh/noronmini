package com.noronmini.service.mongo;

import com.noronmini.jooq.tables.pojos.Topic;
import com.noronmini.mapper.PostMapper;
import com.noronmini.mapper.TopicMapper;
import com.noronmini.mongo.pojo.Post;
import com.noronmini.repo.PostRepo;
import com.noronmini.repo.TopicRepo;
import com.noronmini.request.PostRequest;
import com.noronmini.response.PostResponse;
import com.noronmini.response.TopicName;
import com.noronmini.response.UserResponse;
import com.noronmini.service.ESService;
import com.noronmini.service.mysql.UserService;
import com.noronmini.service.producer.NoronProducer;
import com.noronmini.utils.ServiceUtils;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class PostService extends BaseService<PostResponse, PostRequest, Integer, Post> {

    private final PostRepo postRepo;
    private final UserService userService;
    private final TopicRepo topicRepo;
    private final NoronProducer noronProducer;
    private final TopicMapper topicMapper;
    private final ESService esService;

    public PostService(PostRepo postRepo
            , UserService userService
            , TopicRepo topicRepo
            , PostMapper postMapper
            , NoronProducer noronProducer
            , TopicMapper topicMapper
            , ESService esService) {
        this.userService = userService;
        this.topicMapper = topicMapper;
        this.esService = esService;
        baseRepo = postRepo;
        this.postRepo = postRepo;
        this.topicRepo = topicRepo;
        mapper = postMapper;
        this.noronProducer = noronProducer;
    }

    @Override
    public List<PostResponse> getAll() {
        List<PostResponse> postResponses = super.getAll();
        Map<Integer, TopicName> mapTopic = getTopicMap(postResponses);
        Map<Integer, UserResponse> mapUser = getUserMap(postResponses);
        return postResponses.stream()
                .map(post -> {
                    List<TopicName> topics = post.getTopicIds().stream()
                            .map(e -> mapTopic.get(e))
                            .collect(Collectors.toList());
                    post.setTopics(topics);
                    post.setCreator(mapUser.get(post.getCreatorId()));
                    return post;
                })
                .collect(Collectors.toList());
    }

    private Map<Integer, TopicName> getTopicMap(List<PostResponse> posts) {
        Set<Integer> topicIds = new HashSet<>();
        posts.stream()
                .forEach(e -> topicIds.addAll(e.getTopicIds()));
        List<Topic> topics = topicRepo.findByIds(topicIds);
        return topics.stream()
                .map(topic -> topicMapper.toTopicName(topic))
                .collect(Collectors.toMap(TopicName::getId, e -> e));
    }


    private Map<Integer, UserResponse> getUserMap(List<PostResponse> posts) {
        Set<Integer> userIds = posts
                .stream()
                .map(PostResponse::getCreatorId)
                .collect(Collectors.toSet());
        return ServiceUtils.toMapUser(userService, userIds);
    }

    @Override
    public PostResponse insertOne(PostRequest postRequest) {
        PostResponse commentResponse = super.insertOne(postRequest);
        noronProducer.publishPost(postRequest, "post_insert");
//        esService.postNewPost(postRepo.findById(postRequest.getId()));
        return commentResponse;
    }

    @Override
    public int deleteById(Integer id) {
        Post post = postRepo.findById(id);
        int res = super.deleteById(id);
//        esService.deletePost(id);
        noronProducer.publishPost(post, "post_delete");
        return res;
    }

//    public void publishMessage(PostRequest postRequest) {
//        publishThread(postRequest).start();
//    }

//    private Thread publishThread(PostRequest postRequest) {
//        Thread runnable = new Thread(() -> {
//            NoronMessage noronMessage = new NoronMessage()
//                    .setObjectType("Comment")
//                    .setActionType("insert")
//                    .setCreatedAt(LocalDateTime.now())
//                    .setPostId(postRequest.getId())
//                    .setTopicId(postRequest.getTopicIds());
//            noronProducer.publishMessage(noronMessage);
//        });
//        return runnable;
//    }

//    @Override
//    public List<PostResponse> getAll() {
//        List<Post> posts = postRepo.findAll();
//        Set<Integer> userIds = posts
//                .stream()
//                .map(Post::getCreatorId)
//                .collect(Collectors.toSet());
//        Map<Integer, User> mapUser = ServiceUtils.toMapUser(userRepo, userIds);
//        Set<Integer> topicIds = new HashSet<>();
//        posts.stream()
//                .forEach(e -> topicIds.addAll(e.getTopicIds()));
//        List<Topic> topics = topicRepo.findByIds(topicIds);
//        Map<Integer, Topic> mapTopic = topics.stream()
//                .collect(Collectors.toMap(Topic::getId, e -> e));
//        return postMapper.toPostResponses(posts, mapUser, mapTopic);
//    }

//    @Override
//    public PostResponse insertOne(PostRequest postRequest) {
//        Post post = postMapper.toPost(postRequest);
//        post.setCreatedAt(new Date().getTime());
//        post.setNumComments(0);
//        post.setNumLikes(0);
//        post.setNumViews(0);
//        postRepo.insertOne(post);
//        publishMessage(post).start();
//        return toResponse(post.getId());
//    }

//    private Thread publishMessage(Post post) {
//        Thread thread = new Thread(() -> {
//            BaseKafka<Post> baseKafka = new BaseKafka<>();
//            baseKafka.setObjectType("Post");
//            baseKafka.setActionType("insert");
//            baseKafka.setCreatedAt(new Date());
//            baseKafka.setPayload(post);
//            noronProducer.publishPost(baseKafka);
//        });
//        return thread;
//    }

//    @Override
//    public PostResponse updateById(PostRequest postRequest, Integer id) {
//        Post post = postMapper.toPost(postRequest);
//        post.setUpdatedAt(new Date().getTime());
//        postRepo.updateById(post, id);
//        return toResponse(post.getId());
//    }

//    @Override
//    public PostResponse getById(Integer integer) {
//        return null;
//    }
}
