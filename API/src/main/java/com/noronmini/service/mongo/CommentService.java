package com.noronmini.service.mongo;

import com.noronmini.mapper.CommentMapper;
import com.noronmini.mongo.pojo.Comment;
import com.noronmini.repo.CommentRepo;
import com.noronmini.request.CommentRequest;
import com.noronmini.response.CommentResponse;
import com.noronmini.response.UserResponse;
import com.noronmini.service.mysql.UserService;
import com.noronmini.service.producer.NoronProducer;
import com.noronmini.utils.ServiceUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

@Service
public class CommentService extends BaseService<CommentResponse, CommentRequest, Integer, Comment> {

    private final CommentRepo commentRepo;
    private final UserService userService;
    private final NoronProducer noronProducer;

    public CommentService(CommentRepo commentRepo,
                          CommentMapper commentMapper,
                          UserService userService, NoronProducer noronProducer) {
        this.commentRepo = commentRepo;
        this.userService = userService;
        baseRepo = commentRepo;
        mapper = commentMapper;
        this.noronProducer = noronProducer;
    }

    @Override
    public List<CommentResponse> getAll() {
        List<CommentResponse> responses = super.getAll();
        Map<Integer, UserResponse> map = getUserMap(responses);
        return responses.stream()
                .map(e -> e.setCreator(map.get(e.getCreatorId())))
                .collect(toList());
    }

    private Map<Integer, UserResponse> getUserMap(List<CommentResponse> comments) {
        Set<Integer> ids = comments.stream()
                .map(CommentResponse::getCreatorId)
                .collect(toSet());
        return ServiceUtils.toMapUser(userService, ids);
    }

    @Override
    public CommentResponse insertOne(CommentRequest commentRequest) {
        CommentResponse commentResponse = super.insertOne(commentRequest);
        noronProducer.publishComment(commentRequest, "comment_insert");
        return commentResponse;
    }

    @Override
    public int deleteById(Integer id) {
        Comment comment = commentRepo.findById(id);
        int res = super.deleteById(id);
        noronProducer.publishComment(comment, "comment_delete");
        return res;
    }

//    @Override
//    public List<CommentResponse> getAll() {
//        List<Comment> comments = commentRepo.findAll();
//        Set<Integer> ids = comments.stream()
//                .map(Comment::getCreatorId)
//                .collect(Collectors.toSet());
//        Map<Integer, User> map = ServiceUtils.toMapUser(userRepo, ids);
//        return commentMapper.toListResponse(comments, map);
//    }
//
//    @Override
//    public CommentResponse insertOne(CommentRequest commentRequest) {
//        Comment comment = commentMapper.toComment(commentRequest);
//        comment.setCreatedAt(new Date().getTime());
//        comment.setNumLikes(0);
//        if (comment.getParentId() == null) comment.setParentId(-1);
//        commentRepo.insertOne(comment);
//        publishMessage(comment).start();
//        return commentMapper.toCommentResponse(comment, userRepo.findById(comment.getCreatorId()));
//    }
//
//    @Override
//    public CommentResponse updateById(CommentRequest commentRequest, Integer integer) {
//        return null;
//    }
//
//    @Override
//    public int deleteById(Integer integer) {
//        return 0;
//    }

//    @Override
//    public CommentResponse getById(Integer integer) {
//        return null;
//    }

}
