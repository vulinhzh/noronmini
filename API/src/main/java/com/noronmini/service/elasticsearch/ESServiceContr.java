package com.noronmini.service.elasticsearch;

import com.noronmini.es.KeyWord;
import com.noronmini.response.PostResponse;
import com.noronmini.service.ESService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ESServiceContr {

    private final ESService esService;

    public ESServiceContr(ESService esService) {
        this.esService = esService;
    }

    public PostResponse getOne(String id) {
        return esService.getById(id);
    }

    public List<PostResponse> searchPost(KeyWord keyWord) {
        return esService.searchByKeyword(keyWord.getKeyword(), keyWord.getField());
    }

}
