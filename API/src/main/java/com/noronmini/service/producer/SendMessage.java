//package com.noronmini.service.producer;
//
//import com.noronmini.kafkadata.NoronMessage;
//import com.noronmini.response.CommentResponse;
//import org.aspectj.lang.JoinPoint;
//import org.aspectj.lang.annotation.AfterReturning;
//import org.aspectj.lang.annotation.Aspect;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Component;
//
//import java.time.LocalDateTime;
//
//@Aspect
//@Component
//public class SendMessage {
//
//    @AfterReturning(pointcut = "execution(* com.noronmini.controller.CommentController.addNew(..))", returning = "result")
//    public void afterReturning(JoinPoint joinPoint, Object result) {
//        CommentResponse commentResponse = (CommentResponse) ((ResponseEntity) result).getBody();
//        NoronMessage noronMessage = new NoronMessage()
//                .setObjectType("Comment")
//                .setActionType("insert")
//                .setCommentId(commentResponse.getId())
//                .setCreatedAt(LocalDateTime.now())
//                .setPostId(commentResponse.get)
//    }
//}
