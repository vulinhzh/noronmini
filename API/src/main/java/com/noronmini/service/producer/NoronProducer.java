package com.noronmini.service.producer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.noronmini.kafkadata.NoronMessage;
import com.noronmini.mapper.MessageMapper;
import com.noronmini.mongo.pojo.Comment;
import com.noronmini.mongo.pojo.Post;
import com.noronmini.request.CommentRequest;
import com.noronmini.request.PostRequest;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class NoronProducer {

    private final String topic = "noronmini";
    private final KafkaTemplate<String, String> kafkaTemplate;
    private final MessageMapper messageMapper;
    private ObjectMapper objectMapper = new ObjectMapper();

    public NoronProducer(KafkaTemplate<String, String> kafkaTemplate, MessageMapper messageMapper) {
        this.kafkaTemplate = kafkaTemplate;
        this.messageMapper = messageMapper;
    }

    public void publishPost(PostRequest post, String actionType) {
        NoronMessage noronMessage = messageMapper.toMessage(post, actionType);
        sendMessage(noronMessage);
    }

    public void publishPost(Post post, String actionType) {
        NoronMessage noronMessage = messageMapper.toMessage(post, actionType);
        sendMessage(noronMessage);
    }

    public void publishComment(CommentRequest comment, String actionType) {
        NoronMessage noronMessage = messageMapper.toMessage(comment, actionType);
        sendMessage(noronMessage);
    }

    public void publishComment(Comment comment, String actionType) {
        NoronMessage noronMessage = messageMapper.toMessage(comment, actionType);
        sendMessage(noronMessage);
    }

    private void sendMessage(NoronMessage noronMessage) {

        String json = "";
        try {
            json = objectMapper.writeValueAsString(noronMessage);
            ProducerRecord<String, String> record = new ProducerRecord<>(topic, json);
            if (record != null)
                new Thread(() -> kafkaTemplate.send(record)).start();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
