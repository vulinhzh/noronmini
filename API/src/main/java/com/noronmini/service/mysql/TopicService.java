package com.noronmini.service.mysql;

import com.noronmini.jooq.tables.pojos.Topic;
import com.noronmini.jooq.tables.pojos.TopicUser;
import com.noronmini.jooq.tables.records.TopicRecord;
import com.noronmini.mapper.TopicMapper;
import com.noronmini.repo.TopicRepo;
import com.noronmini.repo.TopicUserRepo;
import com.noronmini.request.TopicRequest;
import com.noronmini.response.TopicResponse;
import com.noronmini.response.UserResponse;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class TopicService extends BaseService<TopicResponse, TopicRequest, Integer, Topic, TopicRecord> {

    private final TopicRepo topicRepo;
    private final TopicUserRepo topicUserRepo;
    private final UserService userService;

    public TopicService(TopicRepo topicRepo, TopicMapper topicMapper,
                        TopicUserRepo topicUserRepo, UserService userService) {
        baseRepo = topicRepo;
        this.topicRepo = topicRepo;
        mapper = topicMapper;
        this.topicUserRepo = topicUserRepo;
        this.userService = userService;
    }

//    @Override
//    public List<TopicResponse> getAll() {
//        return topicRepo.findAll()
//                .stream()
//                .map(e -> topicMapper.topicResponse(e))
//                .collect(Collectors.toList());
//    }
//
//    @Override
//    public TopicResponse insertOne(TopicRequest topicRequest) {
//        Topic newTopic = topicMapper.toTopic(topicRequest);
//        newTopic.setCreatedAt(LocalDateTime.now());
//        return topicMapper.topicResponse(topicRepo.insertOne(newTopic));
//    }

//    @Override
//    public TopicResponse updateById(TopicRequest topicRequest, Integer id) {
//        Topic newTopic = topicMapper.toTopic(topicRequest);
//        newTopic.setUpdatedAt(LocalDateTime.now());
//        topicRepo.updateById(newTopic, id);
//        return getById(id);
//    }

//    @Override
//    public int deleteById(Integer id) {
//        return topicRepo.deleteById(id);
//    }

//    public TopicResponse getById(Integer id) {
//        return topicMapper.topicResponse(topicRepo.findById(id));
//    }

    /*
        - add follower
        - remove follower
        - get follower
     */
    public TopicUser addFollower(TopicUser topicUser) {
        topicUser.setCreatedAt(LocalDateTime.now());
        return topicUserRepo.insertOne(topicUser);
    }

    public int removeFollower(Integer topicId, Integer userId) {
        return topicUserRepo.deleteById(userId, topicId);
    }

    public List<UserResponse> getFollowerByPostId(Integer postId) {
        Set<Integer> userIDs = topicUserRepo.findByTopicId(postId)
                .stream()
                .map(TopicUser::getUserId)
                .collect(Collectors.toSet());
        return userService.getAllByIDs(userIDs);
    }

    public List<TopicResponse> getAllByIDs(Set<Integer> ids) {
        return topicRepo.findByIds(ids)
                .stream()
                .map(e -> mapper.toResponse(e))
                .collect(Collectors.toList());
    }
}
