package com.noronmini.service.mysql;

import com.noronmini.mapper.AbstractMapper;
import com.noronmini.repo.BaseRepo;
import com.noronmini.service.IService;
import org.jooq.Record;

import javax.annotation.PostConstruct;
import java.util.List;

public abstract class BaseService<Rs, Rq, ID, P, R extends Record> implements IService<Rs, Rq, ID> {
    protected BaseRepo<R, P, ID> baseRepo;
    protected AbstractMapper<P, Rq, Rs> mapper;

    @PostConstruct
    void init() {
        System.out.println();
    }

    @Override
    public List<Rs> getAll() {
        List<P> list = baseRepo.findAll();
        System.out.println();
        return mapper.toListResponse(list);
    }

    @Override
    public Rs insertOne(Rq q) {
        P d = baseRepo.insertOne(mapper.toPOJO(q));
        return mapper.toResponse(d);
    }

    @Override
    public Rs updateById(Rq q, ID id) {
        P d = mapper.toPOJO(q);
        baseRepo.updateById(d, id);
        return mapper.toResponse(baseRepo.findById(id));
    }

    @Override
    public int deleteById(ID id) {
        baseRepo.deleteById(id);
        return 1;
    }

    @Override
    public Rs getById(ID id) {
        return mapper.toResponse(baseRepo.findById(id));
    }
}
