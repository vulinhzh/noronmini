package com.noronmini.service.mysql;

import com.noronmini.jooq.tables.pojos.User;
import com.noronmini.jooq.tables.records.UserRecord;
import com.noronmini.mapper.UserMapper;
import com.noronmini.repo.UserRepo;
import com.noronmini.request.UserRequest;
import com.noronmini.response.UserResponse;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserService extends BaseService<UserResponse, UserRequest, Integer, User, UserRecord> {

    private final UserRepo userRepo;

    public UserService(UserRepo userRepo, UserMapper userMapper) {
        this.userRepo = userRepo;
        baseRepo = userRepo;
        mapper = userMapper;
    }

//    @Override
//    public List<UserResponse> getAll() {
//        return userRepo.findAll()
//                .stream()
//                .map(e -> userMapper.toResponseUser(e))
//                .collect(Collectors.toList());
//    }

//    @Override
//    public UserResponse insertOne(UserRequest userRequest) {
//        User newUser = userRepo.insertOne(userMapper.toUser(userRequest));
//        newUser.setCreatedAt(LocalDateTime.now());
//        return userMapper.toResponseUser(newUser);
//    }

//    @Override
//    public UserResponse updateById(UserRequest userRequest, Integer id) {
//        User newUser = userMapper.toUser(userRequest);
//        newUser.setUpdatedAt(LocalDateTime.now());
//        int i = userRepo.updateById(newUser, id);
//        if (i >= 1) return getById(id);
//        else return null;
//
//    }

//    @Override
//    public int deleteById(Integer id) {
//        userRepo.deleteById(id);
//        return 1;
//    }

//    public UserResponse getById(Integer id) {
//        return userMapper.toResponseUser(userRepo.findById(id));
//    }

    public List<UserResponse> getAllByIDs(Set<Integer> ids) {
        return userRepo.findByIds(ids)
                .stream()
                .map(e -> mapper.toResponse(e))
                .collect(Collectors.toList());
    }
}
