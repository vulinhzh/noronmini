package com.noronmini.utils;

import com.noronmini.jooq.tables.pojos.User;
import com.noronmini.repo.UserRepo;
import com.noronmini.response.UserResponse;
import com.noronmini.service.mysql.UserService;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ServiceUtils {
    public static Map<Integer, UserResponse> toMapUser(UserService userService, Set<Integer> ids) {
        List<UserResponse> users = userService.getAllByIDs(ids);
        Map<Integer, UserResponse> map = users.stream()
                .collect(Collectors.toMap(UserResponse::getId, Function.identity()));
        return map;
    }

}
