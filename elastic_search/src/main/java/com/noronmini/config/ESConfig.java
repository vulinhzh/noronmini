package com.noronmini.config;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.transport.ElasticsearchTransport;
import co.elastic.clients.transport.rest_client.RestClientTransport;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ESConfig {

    @Bean
    public RestClient restClient() {
//        RestClientBuilder restClient = RestClient.builder(
//                new HttpHost("noronmini.kb.us-central1.gcp.cloud.es.io", 9243));
//        Header[] defaultHeaders =
//                new Header[]{new BasicHeader("Authorization",
//                        "Bearer u6iuAxZ0RG1Kcm5jVFI4eU4tZU9aVFEwT2F3")};
        final CredentialsProvider credentialsProvider =
                new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY,
                new UsernamePasswordCredentials("elastic", "c6z5IuLBwbMe7ajWu04c035U"));

        RestClientBuilder builder = RestClient.builder(
                        new HttpHost("noronmini.es.us-central1.gcp.cloud.es.io"
                                , 9243, "https"))
                .setHttpClientConfigCallback(httpClientBuilder -> httpClientBuilder
                        .setDefaultCredentialsProvider(credentialsProvider));
        return builder.build();
    }

    @Bean
    public ElasticsearchTransport transport() {
        ElasticsearchTransport transport = new RestClientTransport(
                restClient(), new JacksonJsonpMapper());
        return transport;
    }

    @Bean
    public ElasticsearchClient client() {
        ElasticsearchClient client = new ElasticsearchClient(transport());
        return client;
    }
}
