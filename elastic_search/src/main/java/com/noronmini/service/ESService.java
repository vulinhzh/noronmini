package com.noronmini.service;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.aggregations.Aggregate;
import co.elastic.clients.elasticsearch._types.query_dsl.MatchQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import co.elastic.clients.elasticsearch._types.query_dsl.RangeQuery;
import co.elastic.clients.elasticsearch.core.*;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.json.JsonData;
import com.noronmini.kafkadata.NoronMessage;
import com.noronmini.mapper.PostMapper;
import com.noronmini.mongo.pojo.Post;
import com.noronmini.repo.PostRepo;
import com.noronmini.response.PostResponse;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ESService {

    private final
    ElasticsearchClient client;

    private final PostMapper mapper;
    private final PostRepo postRepo;

    public ESService(ElasticsearchClient client, PostMapper mapper
            , PostRepo postRepo) {
        this.client = client;
        this.mapper = mapper;
        this.postRepo = postRepo;
    }

    public void postNewPost(Post post) {
        PostResponse postResponse = mapper.toResponse(post);
        try {
            IndexResponse indexResponse = client.index(builder -> builder
                    .index("post")
                    .id(String.valueOf(post.getId()))
                    .document(postResponse)
            );
            System.out.println();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void postManyPost(List<NoronMessage> noronMessages) {
        noronMessages.stream()
                .map(e -> postRepo.findById(e.getPostId()))
                .forEach(e -> postNewPost(e));
    }

    public void postWithBulk(List<NoronMessage> noronMessages) {
        BulkRequest.Builder builder = new BulkRequest.Builder();
        noronMessages.stream()
                .map(e -> postRepo.findById(e.getPostId()))
                .forEach(e -> builder.operations(
                        oper -> oper.index(
                                idx -> idx
                                        .index("post")
                                        .id(String.valueOf(e.getId()))
                                        .document(mapper.toResponse(e)))));
        try {
            BulkResponse bulkResponse = client.bulk(builder.build());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void deletePost(Integer id) {
        try {
            DeleteResponse deleteResponse = client.delete(builder -> builder.index("post").id(String.valueOf(id)));
            System.out.println();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void deleteManyPost(List<NoronMessage> noronMessages) {
        noronMessages.forEach(e -> deletePost(e.getPostId()));
    }

    public void deleteWithBulk(List<NoronMessage> noronMessages) {
        BulkRequest.Builder builder = new BulkRequest.Builder();
        noronMessages.stream()
                .map(e -> postRepo.findById(e.getPostId()))
                .forEach(e -> builder.operations(
                        oper -> oper.delete(
                                idx -> idx
                                        .index("post")
                                        .id(String.valueOf(e.getId())))));
        try {
            BulkResponse bulkResponse = client.bulk(builder.build());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public PostResponse getById(String id) {
        try {
            GetResponse<PostResponse> getResponse = client.get(builder -> builder
                    .index("post")
                    .id(id), PostResponse.class);
            if (getResponse.found()) return getResponse.source();
            else return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<PostResponse> searchByKeyword(String keyword, String field) {
        try {
            SearchResponse<PostResponse> response = client.search(
                    builder -> builder
                            .index("post")
                            .query(q -> q.match(
                                    mapper -> mapper
                                            .field(field)
                                            .query(keyword)
                                            .minimumShouldMatch("2")
                                            .prefixLength(2)
                            )), PostResponse.class);
            return response.hits().hits().stream()
                    .map(Hit::source)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<PostResponse> searchByKeyword2(String keyword, String field) {
        try {
            Query query1 = MatchQuery.of(builder -> builder.field(field).query(keyword))._toQuery();
            Query query2 = RangeQuery.of(builder -> builder.field("id").gte(JsonData.of(2)))._toQuery();
            SearchResponse<PostResponse> response = client.search(
                    builder -> builder
                            .index("post")
                            .query(q -> q.bool(
                                    b -> b.must(query1)
                                            .must(query2)
                            )), PostResponse.class);
            return response.hits().hits().stream()
                    .map(Hit::source)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Integer count(String key, String field) {
        try {
            SearchResponse<PostResponse> response = client.search(builder -> builder
                            .index("post")
                            .query(q ->
                                    q.match(mapper -> mapper
                                            .field(field)
                                            .query(key)))
                            .aggregations("count", agg -> agg
                                    .valueCount(c -> c.field("id")))
                    , PostResponse.class);
            Map<String, Aggregate> map = response.aggregations();
            map.get("count");
            return 0;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
